import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ProjetsListComponent } from './projets-list/projets-list.component';
import { ProjetDetailComponent } from './projet-detail/projet-detail.component';
import { AddProjetComponent } from './add-projet/add-projet.component';
import { UpdateProjetComponent } from './update-projet/update-projet.component';
import { AddTicketComponent } from './add-ticket/add-ticket.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'projets', component: ProjetsListComponent},
  {path: 'projets/add', component: AddProjetComponent},
  {path: 'projets/:id/edit', component: UpdateProjetComponent},
  {path: 'projets/:id', component: ProjetDetailComponent},  
  {path: 'projets/:id/addTicket', component: AddTicketComponent},
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
