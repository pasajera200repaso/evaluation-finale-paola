import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { ProjetsListComponent } from './projets-list/projets-list.component';
import { HomeComponent } from './home/home.component';
import { HttpClientModule } from '@angular/common/http';
import { ProjetDetailComponent } from './projet-detail/projet-detail.component';
import { AddProjetComponent } from './add-projet/add-projet.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FieldModule } from './field/field.module';
import { UpdateProjetComponent } from './update-projet/update-projet.component';
import { AddTicketComponent } from './add-ticket/add-ticket.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ProjetsListComponent,
    HomeComponent,
    ProjetDetailComponent,
    AddProjetComponent,
    UpdateProjetComponent,
    AddTicketComponent, 
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,   
     HttpClientModule,FormsModule, FieldModule, ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
