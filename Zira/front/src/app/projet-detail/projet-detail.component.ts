import { Component, OnInit } from '@angular/core';
import { Projet } from '../models/projet';
import { ProjetService } from '../services/projet.service';
import { TicketService } from '../services/ticket.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Ticket } from '../models/ticket';

@Component({
  selector: 'app-projet-detail',
  templateUrl: './projet-detail.component.html',
  styleUrls: ['./projet-detail.component.css']
})
export class ProjetDetailComponent  implements OnInit {
  [x: string]: any;

  projetDetail: Projet | undefined; 

  id?: number;

  projet?: Projet;

  ticket?: Ticket[] =[];


  constructor(public projetService: ProjetService, public ticketService: TicketService, private router: Router, private route: ActivatedRoute){}



  ngOnInit(): void {
    this.route.params.subscribe((params: Params): void => {
      const id = Number(params['id']); 
        this.projetService.getProjetDetail(id).subscribe(projet => 
        this.projetDetail = projet
        
        ); 
        
     
    })
  }

  deleteProjet():void {
    this.id = this.route.snapshot.paramMap.get('id') as unknown as number;
    this.projetService.delete(this.id).subscribe(() => {
      const currentUrl = this.router.url;
        this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
          this.router.navigate(['projets']);
        });
    
      })
    };

    deleteTicket():void {
      this.id = this.route.snapshot.paramMap.get('id') as unknown as number;
      this.ticketService.delete(this.id).subscribe(() => {
        const currentUrl = this.router.url;
          this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
            this.router.navigate(['tickets']);
          });
      
        })
      };
}


