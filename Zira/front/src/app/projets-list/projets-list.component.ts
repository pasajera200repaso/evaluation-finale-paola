import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Projet } from '../models/projet';
import { ProjetService } from '../services/projet.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-projets-list',
  templateUrl: './projets-list.component.html',
  styleUrls: ['./projets-list.component.css']
})
export class ProjetsListComponent implements OnInit {

  projets: Projet[] = [];

  constructor(public projetService: ProjetService, private route: ActivatedRoute){}

  ngOnInit(): void {
    this.projetService.getProjets().subscribe(response => {
      this.projets = response; 
    })
  }

 
}










  // projets: Projet[] = [];

  // constructor(public projetService: ProjetService, private route: ActivatedRoute){}

  // selectProjetsParent(projet: Projet){
  //   this.projetService.getProjets().subscribe(response => {
  //     this.projets = response; 
  //   })
  // }


