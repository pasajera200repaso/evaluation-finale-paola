import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Ticket } from '../models/ticket';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TicketService {
  [x: string]: any;

  private API_URL = "/api"; 

tickets: Ticket[] = new Array<Ticket>();

  constructor(private http: HttpClient) { }

  getTickets(): Observable<Ticket[]>{
    return this.http.get<Ticket[]>(this.API_URL + '/tickets');
  }

  getTicketDetail(id:number):Observable<Ticket> {
    return this.http.get<Ticket>(this.API_URL + '/' + id);
  };

  addTicket(ticket:Ticket):Observable<Ticket> {
    return this.http.post<Ticket>(this.API_URL + '/projets/id', ticket);
  };

  updateTicket(ticket:Ticket):Observable<Ticket> {
    return this.http.put<Ticket>(this.API_URL+ '/projets/id', '/tickets/' + '/id/');
  };

  delete(id: number ):Observable<Ticket> {
    return this.http.delete<Ticket>(this.API_URL + '/tickets/' + id);
  };
  

}
