import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Projet } from '../models/projet';
import { Observable, tap } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProjetService {
  [x: string]: any;



  private API_URL = "/api"; 

  projets: Projet[] = new Array<Projet>();

  constructor(private http: HttpClient) { }

  getProjets(): Observable<Projet[]>{
    return this.http.get<Projet[]>(this.API_URL + '/projets');
  }

  
 getProjetDetail(id: number): Observable<Projet>{
  return this.http.get<Projet>(this.API_URL + '/projets/'+ id); 
 }

 addProjet(projet: Projet): Observable<any> {
  return this.http.post(this.API_URL + '/projets', projet); 
 }

 updateProjet(projet: Projet): Observable<any> {
  return this.http.put(this.API_URL + '/projets', projet); 
 }


 delete(id: number): Observable<Projet> {
  return this.http.delete<Projet>(this.API_URL + '/projets/'+ id)
}

}
