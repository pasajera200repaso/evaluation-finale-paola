import { Component, OnInit } from '@angular/core';
import { ProjetService } from '../services/projet.service';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Projet } from '../models/projet';

@Component({
  selector: 'app-add-projet',
  templateUrl: './add-projet.component.html',
  styleUrls: ['./add-projet.component.css']
})
export class AddProjetComponent implements OnInit{

 constructor(
  private projetService: ProjetService, private router: Router
 ){}
  ngOnInit(): void {
    throw new Error('Method not implemented.');
  }

  myForm = new FormGroup({
  name: new FormControl('', [Validators.required]),
  statement: new FormControl('', [Validators.required]),
  })

  save():void {    

    if (this.myForm.valid) {
      console.log(this.myForm)
      const values = this.myForm.value 
      const result = {...values} as unknown as Projet
      this.projetService.addProjet(result).subscribe(() => {

        const actuelleUrl = this.router.url;
        this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
          this.router.navigate(['/']);
        });
      })
    }
  }

}

