import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Projet } from '../models/projet';
import { ProjetService } from '../services/projet.service';

@Component({
  selector: 'app-update-projet',
  templateUrl: './update-projet.component.html',
  styleUrls: ['./update-projet.component.css']
})
export class UpdateProjetComponent implements OnInit {
  
  id!: number;

  constructor(private projetService: ProjetService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id') as unknown as number;
    this.projetService.getProjetDetail(this.id).subscribe((projet) => {
      if (projet) {
        this.myForm.patchValue({
          name: projet.name,
          statement: projet.statement
        });
      };
    });
  };

  myForm = new FormGroup({
    name: new FormControl('', [Validators.required]),
   statement: new FormControl('', [Validators.required]),
  })

  update():void {    
    if (this.myForm.valid) {
      
      const values = this.myForm.value;
      const result = {...values} as unknown as Projet;
      this.projetService.updateProjet(result).subscribe(() => {
      
        const currentUrl = this.router.url;
        this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
          this.router.navigate(['/']);
        });
      });
    }
  }



}
