import { Ticket } from "./ticket";

export class Projet {
    constructor(
        public id: number,
        public name: string, 
        public statement: string, 
        public creationDate: number, 
        public ticketList: Ticket[]

    ){}
}
