export class Ticket {
    constructor(
        public id: number,
        public title: string,
        public content: string, 
        public creationDate: number, 
        public status: string

    ){

    }

}
