import { Component, OnInit } from '@angular/core';
import { TicketService } from '../services/ticket.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Ticket } from '../models/ticket';

@Component({
  selector: 'app-add-ticket',
  templateUrl: './add-ticket.component.html',
  styleUrls: ['./add-ticket.component.css']
})
export class AddTicketComponent implements OnInit {

  
  id?: Number;
  projetId!: string; 
  
  constructor(private ticketService: TicketService,private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.id = Number(this.route.snapshot.paramMap.get('id'));
  }

  myForm = new FormGroup({
    title: new FormControl('', [Validators.required]),
    content: new FormControl('', [Validators.required]),
  })

  save():void {    
    if (this.myForm.valid) {
      
      const values = this.myForm.value 
      const result = {...values, projetId: this.id} as unknown as Ticket
      this.ticketService.addTicket(result).subscribe(() => {

        const actuelleUrl = this.router.url;
        this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
          this.router.navigate(['/' + this.id]);
        });
      })
    }
  }

}
