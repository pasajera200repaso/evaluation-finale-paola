import { Component, OnInit } from '@angular/core';
import { Ticket } from '../models/ticket';
import { TicketService } from '../services/ticket.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-tickets-list',
  templateUrl: './tickets-list.component.html',
  styleUrls: ['./tickets-list.component.css']
})
export class TicketsListComponent implements OnInit {

  tickets: Ticket[] = [];

  constructor(public ticketService: TicketService, private route: ActivatedRoute){}

  ngOnInit(): void {
    this.ticketService.getTickets().subscribe(response => {
      this.tickets = response; 
    })
  }

 

}
