--Zira Script
INSERT INTO projets (name, statement)
VALUES ('Quiz', 'API quiz application');

INSERT INTO projets (name, statement)
VALUES ('Movies', 'SPA with Angular');

INSERT INTO projets (name, statement)
VALUES ('Pet Clinic', 'API REST');

INSERT INTO projets (name, statement)
VALUES ('Kaamelott', 'API application');

INSERT INTO tickets (title, projet_id, content)
VALUES ('Entities definitions', 1, 'Definition des entities');

INSERT INTO tickets (title, projet_id, content, status)
VALUES ('Connection à la base de données', 1, 'Connection à la base de données', 'INPROGRESS');

INSERT INTO tickets (title, projet_id, content)
VALUES ('Conteneuritation', 2, 'Docker compose et volumes');

INSERT INTO tickets (title, projet_id, content)
VALUES ('Decoupage de components', 2, 'Decoupage de components et creation de pages avec le routage');

INSERT INTO tickets (title, projet_id, content)
VALUES ('Entities definitions', 3, 'Definition des entities');

INSERT INTO tickets (title, projet_id, content)
VALUES ('Entities definitions', 4, 'Definition des entities');
