drop table projets cascade ;
drop table tickets cascade;

create table if not exists projets
(
    id          bigint primary key GENERATED ALWAYS AS IDENTITY,
    name        text     not null,
    statement   varchar(500)  not null,
    creation_date timestamp with time zone not null default now()
);

create table if not exists tickets
(
    id          bigint primary key GENERATED ALWAYS AS IDENTITY,
    projet_id   bigint not null,
    title       varchar(50)  not null,
    content     varchar(1000)  not null,
    status      TEXT NOT NULL DEFAULT 'TODO',
    FOREIGN KEY (projet_id) REFERENCES projets on delete cascade,
    creation_date timestamp with time zone not null default now()
);
