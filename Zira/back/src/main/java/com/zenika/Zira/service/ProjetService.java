package com.zenika.Zira.service;


import com.zenika.Zira.domain.Projet;
import com.zenika.Zira.domain.exception.NotFoundException;
import com.zenika.Zira.repository.ProjetRepository;
import com.zenika.Zira.repository.TicketRepository;
import jakarta.transaction.Transactional;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.hibernate.Hibernate.get;



@Service
public class ProjetService {


    List<Projet> projetList = new ArrayList<>();


    private final ProjetRepository projetRepository;

    private final TicketRepository ticketRepository;

    public ProjetService(ProjetRepository projetRepository, TicketRepository ticketRepository) {
        this.projetRepository = projetRepository;
        this.ticketRepository = ticketRepository;
    }


    @Transactional
    public Projet getProjet(long projetId){
        return projetRepository.findById(projetId)
                .orElseThrow(() -> new NotFoundException(Projet.class, projetId));
    }


    @Transactional
    public Long createProjet(Projet projet){
        projet.getTicketList().forEach(ticket ->ticketRepository.save(ticket));
        projetRepository.save(projet);
        Projet projet1 = projetRepository.save(projet);
        return projet.getId();
    }

    @Transactional
    public void updateProjet(Projet projet) {
    }
    @Transactional
    public void deleteprojet(Long projetId) {
        Projet projet = projetRepository.findById(projetId)
                .orElseThrow(() -> new NotFoundException(Projet.class, projetId));

        projetRepository.delete(projet);
    }


}
