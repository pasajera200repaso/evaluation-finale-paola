package com.zenika.Zira.repository;
import com.zenika.Zira.domain.Projet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjetRepository extends JpaRepository<Projet, Long> {


}
