package com.zenika.Zira.service;
import com.zenika.Zira.domain.Projet;
import com.zenika.Zira.domain.Ticket;
import com.zenika.Zira.domain.exception.NotFoundException;
import com.zenika.Zira.repository.TicketRepository;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

@Service
public class TicketService {

    private final TicketRepository ticketRepository;


    public TicketService(TicketRepository ticketRepository) {
        this.ticketRepository = ticketRepository;
    }

    @Transactional
    public Ticket getTicket(long ticketId){
        return ticketRepository.findById(ticketId)
                .orElseThrow(() -> new NotFoundException(Projet.class, ticketId));
    }


    @Transactional
    public Long createTicket(Ticket ticket){
        Ticket ticket1 = ticketRepository.save(ticket);
        ticket1.setTitle(ticket.getTitle());
        ticket1.setContent(ticket.getContent());
        ticket1.setCreationDate(ticket.getCreationDate());
        ticket1.setStatus(ticket.getStatus());
        return ticket.getId();
    }
    @Transactional
    public Long updateTicket(Ticket ticket) {
        Ticket ticket1 = ticketRepository.save(ticket);
        ticket1.setTitle(ticket.getTitle());
        ticket1.setContent(ticket.getContent());
        ticket1.setCreationDate(ticket.getCreationDate());
        ticket1.setStatus(ticket.getStatus());
        return ticket.getId();
    }

    @Transactional
    public void deleteTicket(Long ticketId) {
    Ticket ticket = ticketRepository.findById(ticketId)
                .orElseThrow(() -> new NotFoundException(Ticket.class, ticketId));
    ticketRepository.delete(ticket);
    }
}
