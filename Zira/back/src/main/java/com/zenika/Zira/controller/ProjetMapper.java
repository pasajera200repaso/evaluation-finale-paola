package com.zenika.Zira.controller;

import com.zenika.Zira.controller.dto.ProjetDto;
import com.zenika.Zira.domain.Projet;
import org.springframework.stereotype.Component;

import java.util.List;

import static java.util.Arrays.stream;

@Component
public class ProjetMapper {

    TicketMapper ticketMapper;

    public ProjetMapper(TicketMapper ticketMapper) {
        this.ticketMapper = ticketMapper;
    }


    public ProjetDto toDto(Projet projet){
        ProjetDto projetDto = new ProjetDto();
        projetDto.setId(projet.getId());
        projetDto.setName(projet.getName());
        projetDto.setStatement(projet.getStatement());
        projetDto.setCreationDate(projet.getCreationDate());
        projetDto.setTicketList(projet.getTicketList()
                .stream().map(ticketMapper::toDto)
                .toList());


        return projetDto;
    }

    public  List<ProjetDto> toDto(List<Projet> projetList){
        return projetList.stream()
                .map(this::toDto)
                .toList();
    }

    public Projet toModel (ProjetDto projetDto) {
        Projet projet = new Projet();
        projet.setId(projetDto.getId());
        projet.setName(projetDto.getName());
        projet.setStatement(projetDto.getStatement());
        projet.setCreationDate(projetDto.getCreationDate());
        projet.setTicketList(projetDto.getTicketList()
                .stream().map(ticketMapper::toModel)
                .toList());

        return projet;
    }
}
