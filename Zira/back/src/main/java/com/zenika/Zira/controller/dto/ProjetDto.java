package com.zenika.Zira.controller.dto;

import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode(of = "id")
public class ProjetDto {
    private long id;

    private String name;

    private String statement;

    private List<TicketDto> ticketList = new ArrayList<>();

    private LocalDateTime creationDate;

}
