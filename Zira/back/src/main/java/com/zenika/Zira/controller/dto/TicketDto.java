package com.zenika.Zira.controller.dto;

import com.zenika.Zira.domain.Status;
import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.*;

import java.time.LocalDateTime;

@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode(of = "id")
public class TicketDto {

    private long id;

    private String title;

    private String content;

    private LocalDateTime creationDate;

    //Un statut (TODO, INPROGRESS, DONE)
    //Par défaut lors de la création le statut est TODO.
    private Status status;

}
