package com.zenika.Zira.domain;


import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import java.time.LocalDateTime;

@Getter
@Setter
@ToString
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
@Entity
@Table(name = "tickets")
public class Ticket {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "content", nullable = false)
    private String content;

    @Column(name = "creation_date", nullable = false)
    @CreationTimestamp
    private LocalDateTime creationDate;


    //Un statut (TODO, INPROGRESS, DONE)
    //Par défaut lors de la création le statut est TODO.
    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status status;

    /*@ManyToOne(optional = false)
    @JoinColumn(name = "projet_id", nullable = false, insertable=false, updatable=false)
    private Projet projet;
    J'ai commence avec cette aprroche mais j'ai dû abbandonner car cela remontait des erreurs de Stackoverflow. La conclusion c'était que travailler avec des relations bidirectionnelles est plus delicat car il faut gerer les relations de deux entities.
*/
}
