package com.zenika.Zira.controller;

import com.zenika.Zira.controller.dto.ProjetDto;
import com.zenika.Zira.domain.Projet;
import com.zenika.Zira.repository.ProjetRepository;
import com.zenika.Zira.service.ProjetService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/projets")
public class ProjetController {

    private final ProjetService projetService;

    private final ProjetRepository projetRepository;

    private final ProjetMapper projetMapper;

    public ProjetController(ProjetService projetService, ProjetRepository projetRepository, ProjetMapper projetMapper) {
        this.projetService = projetService;
        this.projetRepository = projetRepository;
        this.projetMapper = projetMapper;
    }


    @GetMapping
    public List<ProjetDto> projets(){
        return projetMapper.toDto(projetRepository.findAll());
    }


    @GetMapping("/{id}")
    public ProjetDto getProjetById (@PathVariable() Long id) {
        Projet projet = projetService.getProjet(id);
        return projetMapper.toDto(projet);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Long createProjet(@RequestBody @Validated ProjetDto projetDto) {
        Projet projet = projetMapper.toModel(projetDto);
        projetService.createProjet(projet);
        return projet.getId();
    }

    @PutMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Long updateProjet(@RequestBody @Validated ProjetDto projetDto) {
        Projet projet = projetMapper.toModel(projetDto);
        projetService.updateProjet(projet);
        return projet.getId();
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteProjet(@PathVariable("id") Long projetId) {
        projetService.deleteprojet(projetId);
    }



}
