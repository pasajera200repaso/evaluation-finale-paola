package com.zenika.Zira.controller;

import com.zenika.Zira.controller.dto.TicketDto;
import com.zenika.Zira.domain.Ticket;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TicketMapper {

    public TicketDto toDto(Ticket ticket){
        TicketDto ticketDto = new TicketDto();
        ticketDto.setId(ticket.getId());
        ticketDto.setTitle(ticket.getTitle());
        ticketDto.setContent(ticket.getContent());
        ticketDto.setStatus(ticket.getStatus());
        ticketDto.setCreationDate(ticket.getCreationDate());
        return ticketDto;
    }


    public List<TicketDto> toDto(List<Ticket> ticketList){
        return  ticketList.stream()
                .map(this::toDto)
                .toList();
    }


    public Ticket toModel(TicketDto ticketDto){
        Ticket ticket = new Ticket();
        ticket.setId(ticketDto.getId());
        ticket.setTitle(ticketDto.getTitle());
        ticket.setContent(ticketDto.getContent());
        ticket.setCreationDate(ticketDto.getCreationDate());
        ticket.setStatus(ticketDto.getStatus());
        return ticket;
    }
}



