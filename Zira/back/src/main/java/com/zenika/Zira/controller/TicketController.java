package com.zenika.Zira.controller;
import com.zenika.Zira.controller.dto.ProjetDto;
import com.zenika.Zira.controller.dto.TicketDto;
import com.zenika.Zira.domain.Projet;
import com.zenika.Zira.domain.Ticket;
import com.zenika.Zira.repository.TicketRepository;
import com.zenika.Zira.service.TicketService;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/tickets")
public class TicketController {

    private final TicketService ticketService;

    private final TicketRepository ticketRepository;

    private final TicketMapper ticketMapper;

    public TicketController(TicketService ticketService, TicketRepository ticketRepository, TicketMapper ticketMapper) {
        this.ticketService = ticketService;
        this.ticketRepository = ticketRepository;
        this.ticketMapper = ticketMapper;
    }

    @GetMapping
    public List<TicketDto> tickets(){
        return ticketMapper.toDto(ticketRepository.findAll());
    }

    @GetMapping("/{id}")
    public TicketDto getTicketById (@PathVariable() Long id) {
    Ticket ticket = ticketService.getTicket(id);
        return ticketMapper.toDto(ticket);
    }


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Long createTicket(@RequestBody @Validated TicketDto ticketDto) {
        Ticket ticket = ticketMapper.toModel(ticketDto);
        ticketService.createTicket(ticket);
        return ticket.getId();
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Long updateTicket(@RequestBody @Validated TicketDto ticketDto) {
        Ticket ticket = ticketMapper.toModel(ticketDto);
        ticketService.updateTicket(ticket);
        return ticket.getId();
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTicket(@PathVariable("id") Long ticketId) {
        ticketService.deleteTicket(ticketId);
    }



}
